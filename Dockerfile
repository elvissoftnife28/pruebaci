FROM nginx:alpine

COPY ./dist/Pruebaci/ /usr/share/nginx/html/
CMD ["nginx", "-g", "daemon off;"]
